# Add License

## Roadmap

v0.1.0 -> P.O.C implementation; `init`, `add`, `fetch`, `list`

v0.2.0 -> implement `search`, `preview`

## Todo

- [x] Fetch licenses
    - [x] individually
    - [x] by group
    - [x] with specific source
    - `<cmd> fetch [spdx] [--group <GROUP>]`
- [ ] Add licenses
    - [ ] Input via spdx
    - [ ] User specified directories or default to current working directory
    - [ ] User provided file name or default to `LICENSE`
    - [ ] Add multiple
    - [ ] Fetch on first use, optionally disable
    - `<cmd> add [--no-fetch] [--file-name <NAME>] [--path <PATH>] <spdx>`
- [ ] Search licenses
    - [ ] Fuzzy find
    - [ ] by spdx
    - [ ] by title
    - [ ] by group
    - `<cmd> search [--fzf] [<spdx>] [--title <title>] [--group <group>]`
- [ ] Preview licenses
    - [ ] bat (default)
    - [ ] raw
    - [ ] tldr
        - Summarize; permissions, conditions, limitations
    - [ ] absolute path, to preview somewhere else i.e html in web browser
    - `<cmd> preview [--raw] [--path]`
- [ ] List licenses
    - [ ] individually
    - [ ] by group
    - [ ] groups
    - `<cmd> list [<spdx>] [--group <group>] [--groups]`
- [x] Init
    - [x] Download manifest
    - [x] Prefetch specifed licenses or groups
    - [x] Custom registries to grab licenses
    - [x] User provided manifests or hosted manifests
    - `<cmd> init [--manifest <MANIFEST>] [spdx] [--group <GROUP>]`]

## License

This project is licensed under the [Mozilla Public License Version 2.0](LICENSE)
