// SPDX-License-Identifier: MPL-2.0

use std::{collections::HashMap, fs, sync::Arc};

use anyhow::Context;
use serde::{Deserialize, Serialize};

use crate::{
    data_dir,
    fetch::{fetch_url, Response, UrlFetchType},
};

const REMOTE: &str = "https://gitlab.com/Th3-Wr41th/add-license/-/raw/master/manifest.example.toml";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Manifest {
    pub registries: HashMap<Arc<str>, Arc<str>>,
    pub licenses: Vec<License>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct License {
    pub spdx: Arc<str>,
    pub title: Arc<str>,
    pub groups: Vec<Arc<str>>,
    pub sources: HashMap<Arc<str>, Arc<str>>,
    pub tldr: Tldr,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Tldr {
    pub permissions: Vec<Arc<str>>,
    pub conditions: Vec<Arc<str>>,
    pub limitations: Vec<Arc<str>>,
}

impl Manifest {
    fn validate(&self) -> bool {
        for license in &self.licenses {
            if !license.sources.get("default").is_some() {
                return false;
            }
        }

        true
    }

    pub fn new() -> anyhow::Result<Self> {
        let data_dir = data_dir()?;

        let manifest_file = format!("{data_dir}/manifest.toml");

        info!("Reading manifest from: {manifest_file}");

        let content = fs::read_to_string(&manifest_file).context("failed to read file")?;

        let data: Self = toml::from_str(&content).context("failed to serialize manifest")?;

        Ok(data)
    }

    pub async fn from_url(url: Option<&str>) -> anyhow::Result<Self> {
        let url = url.unwrap_or(REMOTE);

        let response: Response<()> = fetch_url(url, UrlFetchType::Text).await?;

        let Response::Text(data) = response else {
            bail!("response type did not match fetch type");
        };

        let s: Self = toml::from_str(data.as_ref()).context("failed to serialize data")?;

        if !s.validate() {
            error!("invalid manifest");
            debug!("{s:#?}");
            bail!("invalid manifest");
        }

        Ok(s)
    }
}
