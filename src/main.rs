// SPDX-License-Identifier: MPL-2.0

#[macro_use]
extern crate tracing;

#[macro_use]
extern crate anyhow;

mod cli;
mod fetch;
mod init;
mod list;
mod manifest;

use std::{env, fs, path::Path, sync::Arc};

use anyhow::Context;
use clap::Parser;
use tracing::Level;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use cli::{Args, Command};
use fetch::{FetchType, FetchValue};

fn data_dir() -> anyhow::Result<Arc<str>> {
    let dir = if let Ok(home) = env::var("XDG_DATA_HOME") {
        home
    } else if let Ok(home) = env::var("HOME") {
        format!("{home}/.local/share")
    } else {
        bail!("cannot determine data dir");
    };

    let data_dir = format!("{dir}/add-license");

    if !Path::new(&data_dir).exists() {
        fs::create_dir_all(&data_dir).context("failed to create data directory")?;
    }

    Ok(data_dir.into())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let filter = EnvFilter::builder()
        .with_default_directive(Level::DEBUG.into())
        .from_env()?
        .add_directive("hyper=warn".parse()?);

    let subscriber = FmtSubscriber::builder().with_env_filter(filter).finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("setting default subscriber failed")?;

    info!("Tracing started");

    let args = Args::parse();

    match &args.command {
        Command::Init {
            manifest,
            group,
            spdx,
        } => {
            let groups: Vec<FetchValue> = group
                .iter()
                .map(|group| FetchValue::parse(group, FetchType::Group))
                .collect();

            let spdx_ids: Vec<FetchValue> = spdx
                .iter()
                .map(|spdx| FetchValue::parse(spdx, FetchType::Spdx))
                .collect();

            let prefetch: Vec<_> = vec![groups, spdx_ids]
                .iter()
                .flatten()
                .map(|item| item.clone())
                .collect();

            init::init(manifest.as_deref(), prefetch.as_slice()).await?;
        }
        Command::Fetch { group, spdx } => {
            let groups: Vec<FetchValue> = group
                .iter()
                .map(|group| FetchValue::parse(group, FetchType::Group))
                .collect();

            let spdx_ids: Vec<FetchValue> = spdx
                .iter()
                .map(|spdx| FetchValue::parse(spdx, FetchType::Spdx))
                .collect();

            let fetch: Vec<_> = vec![groups, spdx_ids]
                .iter()
                .flatten()
                .map(|item| item.clone())
                .collect();

            fetch::fetch(fetch.as_slice()).await?;
        }
        Command::List {
            group,
            spdx,
            groups,
        } => {
            if *groups {
                list::list_groups()?;
            } else {
                list::list(spdx.as_slice(), group.as_slice())?;
            }
        }
        _ => todo!(),
    }

    debug!("{args:#?}");

    todo!()
}
