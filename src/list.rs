// SPDX-License-Identifier: MPL-2.0

use std::sync::Arc;
use std::{collections::HashMap, fmt::Write};

use anyhow::Context;
use bat::{Input, PagingMode, PrettyPrinter, WrappingMode};

use crate::manifest::{License, Manifest};

pub fn list_groups() -> anyhow::Result<()> {
    info!("Listing groups");

    let manifest = Manifest::new()?;

    let mut groups: HashMap<Arc<str>, Vec<Arc<str>>> = HashMap::new();

    for license in &manifest.licenses {
        if license.groups.is_empty() {
            continue;
        }

        for group in &license.groups {
            if let Some(arr) = groups.get_mut(group) {
                arr.push(Arc::clone(&license.spdx));
            } else {
                groups.insert(Arc::clone(group), vec![Arc::clone(&license.spdx)]);
            }
        }
    }

    let data: Vec<(&str, Vec<u8>)> = groups
        .iter()
        .map(|(group, spdx_ids)| {
            let data = spdx_ids.join("\n\n");

            let bytes = data.as_bytes();

            (group.as_ref(), bytes.to_vec())
        })
        .collect();

    if data.is_empty() {
        bail!("No groups found");
    }

    PrettyPrinter::new()
        .header(true)
        .grid(true)
        .line_numbers(true)
        .wrapping_mode(WrappingMode::Character)
        .paging_mode(PagingMode::QuitIfOneScreen)
        .inputs(
            data.iter()
                .map(|(title, data)| Input::from_bytes(data).kind("Group").title(*title)),
        )
        .print()
        .context("failed to pretty print output")?;

    Ok(())
}

pub fn list(spdx_ids: &[Arc<str>], groups: &[Arc<str>]) -> anyhow::Result<()> {
    info!("Listing licenses");

    let manifest = Manifest::new()?;

    let licenses: Vec<_> = manifest
        .licenses
        .iter()
        .filter(|license| {
            let spdx_match = {
                let name = license.spdx.to_lowercase();

                spdx_ids.iter().any(|id| id.to_lowercase() == name)
            };

            let group_match = { groups.iter().any(|group| license.groups.contains(group)) };

            spdx_match || group_match
        })
        .collect();

    let data: Vec<(&str, Vec<u8>)> = licenses
        .iter()
        .filter_map(|license| match fmt_license(license) {
            Ok(val) => Some((license.title.as_ref(), val.as_bytes().to_vec())),
            Err(_) => None,
        })
        .collect();

    if data.is_empty() {
        bail!("No licenses or groups found");
    }

    PrettyPrinter::new()
        .header(true)
        .grid(true)
        .line_numbers(true)
        .wrapping_mode(WrappingMode::Character)
        .paging_mode(PagingMode::QuitIfOneScreen)
        .inputs(
            data.iter()
                .map(|(title, data)| Input::from_bytes(data).kind("License").title(*title)),
        )
        .print()
        .context("failed to pretty print output")?;

    Ok(())
}

fn fmt_license(license: &License) -> anyhow::Result<Arc<str>> {
    let License {
        spdx,
        groups,
        sources,
        ..
    } = license;

    let indent = "  ";

    let mut s = String::new();

    writeln!(s, "SPDX ID: {spdx}")?;
    writeln!(s)?;

    writeln!(s, "Groups:")?;

    for group in groups {
        writeln!(s, "{indent}{group}")?;
    }
    writeln!(s)?;

    writeln!(s, "Sources:")?;

    for key in sources.keys() {
        writeln!(s, "{indent}{key}")?;
    }
    writeln!(s)?;

    writeln!(s, "Path: '$XDG_DATA_HOME/add-license/licenses/{spdx}'")?;

    Ok(s.into())
}
