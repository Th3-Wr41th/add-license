// SPDX-License-Identifier: MPL-2.0

use std::{fs, path::PathBuf, sync::Arc};

use anyhow::Context;
use reqwest::Url;
use serde::de::DeserializeOwned;

use crate::{
    data_dir,
    manifest::{License, Manifest},
};

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum UrlFetchType {
    Raw,
    Json,
    Text,
}

#[derive(Debug)]
pub enum Response<T: DeserializeOwned> {
    Raw(reqwest::Response),
    Json(T),
    Text(Arc<str>),
}

pub async fn fetch_url<T>(url: &str, ftype: UrlFetchType) -> anyhow::Result<Response<T>>
where
    T: DeserializeOwned,
{
    let url = Url::parse(url).context("failed to parse url")?;

    if url.scheme() == "file" {
        let file = url.path();

        info!("File scheme detected. Attempting to read file: {file}");

        let content = fs::read(file).context("failed to read file")?;

        return match ftype {
            UrlFetchType::Text => {
                let data = String::from_utf8(content).context("failed to parse data")?;

                Ok(Response::Text(data.into()))
            }
            UrlFetchType::Json => {
                let data: T =
                    serde_json::from_slice(&content).context("failed to deserialize data")?;

                Ok(Response::Json(data))
            }
            UrlFetchType::Raw => Err(anyhow!(
                "This fetch type is not supported with file schemes"
            )),
        };
    }

    info!("Attempting to get: {url}");

    let response = reqwest::get(url)
        .await
        .context("failed to make GET request")?;

    match ftype {
        UrlFetchType::Raw => Ok(Response::Raw(response)),
        UrlFetchType::Json => {
            let data: T = response
                .json()
                .await
                .context("failed to parse response as json")?;

            Ok(Response::Json(data))
        }
        UrlFetchType::Text => {
            let data = response
                .text()
                .await
                .context("failed to parse response as text")?;

            Ok(Response::Text(data.into()))
        }
    }
}

#[derive(Debug, Clone)]
pub struct FetchValue {
    pub name: Arc<str>,
    pub source: Arc<str>,
    pub fetch_type: FetchType,
}

impl FetchValue {
    pub fn parse(value: &str, fetch_type: FetchType) -> Self {
        let (value, source) = value.split_once(':').unwrap_or((value, "default"));

        FetchValue {
            name: value.into(),
            source: source.into(),
            fetch_type,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FetchType {
    Spdx,
    Group,
}

fn get_sources(manifest: &Manifest, args: &[FetchValue]) -> Vec<(License, Vec<Arc<str>>)> {
    manifest
        .licenses
        .iter()
        .filter_map(|license| {
            let mut sources: Vec<_> = args
                .iter()
                .filter_map(|arg| {
                    let name = arg.name.to_lowercase();
                    let include = match arg.fetch_type {
                        FetchType::Spdx => name == license.spdx.to_lowercase(),
                        FetchType::Group => {
                            license.groups.contains(&(Into::<Arc<str>>::into(name)))
                        }
                    };

                    if include {
                        Some((Arc::clone(&arg.source), arg.fetch_type))
                    } else {
                        None
                    }
                })
                .collect();

            if sources.len() > 1 {
                sources = sources
                    .iter()
                    .filter(|(source, fetch_type)| match fetch_type {
                        FetchType::Spdx => true,
                        FetchType::Group => Arc::clone(source) != Into::<Arc<str>>::into("default"),
                    })
                    .cloned()
                    .collect();
            }

            let (sources, _): (Vec<_>, Vec<_>) = sources.iter().cloned().unzip();

            if sources.is_empty() {
                None
            } else {
                Some((license.clone(), sources))
            }
        })
        .collect()
}

struct UrlData {
    spdx: Arc<str>,
    source: Arc<str>,
    url: Arc<str>,
}

async fn parse_urls(manifest: &Manifest, urls: Vec<UrlData>) -> anyhow::Result<()> {
    let license_dir = {
        let data_dir = data_dir()?;

        format!("{data_dir}/licenses")
    };

    for data in urls {
        let UrlData { spdx, source, url } = data;

        let license_path = {
            let path = PathBuf::from(license_dir.clone());

            let path = path.join(spdx.as_ref());

            if !path.exists() {
                fs::create_dir_all(&path).context("failed to create license dir")?;
            }

            let filename = format!("{source}.src");

            path.join(&filename)
        };

        if license_path.exists() {
            warn!(
                "{0} license with source: '{1}'; already exists not overriding",
                spdx, source
            );

            continue;
        }

        let Some((scheme, rest)) = url.split_once(":") else {
            continue;
        };

        let url = if let Some(reg_url) = manifest.registries.get(scheme) {
            let reg_url = {
                let last_char = reg_url.chars().last();

                if let Some(char) = last_char {
                    if char == '/' {
                        Arc::clone(reg_url)
                    } else {
                        let mut new = String::from(reg_url.as_ref());

                        new.push('/');

                        new.into()
                    }
                } else {
                    Arc::clone(reg_url)
                }
            };

            let base = Url::parse(reg_url.as_ref()).context("failed to parse url")?;

            let url = base.join(rest)?;

            url
        } else {
            Url::parse(url.as_ref()).context("failed to parse url")?
        };

        let Response::Text(content): Response<()> =
            fetch_url(url.as_str(), UrlFetchType::Text).await?
        else {
            bail!("mismatched response type from 'fetch_url'");
        };

        fs::write(license_path, content.as_ref())
            .context("failed to write data to license file")?;
    }

    Ok(())
}

pub async fn fetch(args: &[FetchValue]) -> anyhow::Result<()> {
    let manifest = Manifest::new()?;

    let sources = get_sources(&manifest, args);

    for (license, sources) in sources {
        let urls: Vec<UrlData> = sources
            .iter()
            .filter_map(|source| {
                let license_src = license.sources.get(source)?;

                Some(UrlData {
                    spdx: Arc::clone(&license.spdx),
                    source: Arc::clone(source),
                    url: Arc::clone(license_src),
                })
            })
            .collect();

        if urls.is_empty() {
            warn!(
                "No urls found for '{0}' with the provided sources: {1:?}",
                license.spdx, sources
            );

            continue;
        }

        info!("Fetching: {0}", license.spdx);

        parse_urls(&manifest, urls).await?
    }

    Ok(())
}
