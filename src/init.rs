// SPDX-License-Identifier: MPL-2.0

use std::fs;

use anyhow::Context;

use crate::{
    data_dir,
    fetch::{self, FetchValue},
    manifest::Manifest,
};

// <cmd> init [--manifest <MANIFEST>] [spdx] [--group <GROUP>]
// <cmd> init --manifest http://example.com/manifest.toml MIT --group common --group gpl

pub async fn init(manifest_url: Option<&str>, prefetch: &[FetchValue]) -> anyhow::Result<()> {
    let data_dir = data_dir()?;

    let manifest = Manifest::from_url(manifest_url).await?;

    let manifest_str = toml::to_string_pretty(&manifest).context("failed to serialize manifest")?;

    let manifest_file = format!("{data_dir}/manifest.toml");

    fs::write(&manifest_file, &manifest_str).context("failed to write manifest file")?;

    fetch::fetch(prefetch)
        .await
        .context("failed to prefetch licenses")?;

    Ok(())
}
