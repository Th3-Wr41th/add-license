// SPDX-License-Identifier: MPL-2.0

use std::sync::Arc;

use clap::{Parser, Subcommand};

#[derive(Debug, Clone, Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Command {
    Init {
        #[arg(short, long)]
        manifest: Option<Arc<str>>,

        #[arg(short, long)]
        group: Vec<Arc<str>>,

        spdx: Vec<Arc<str>>,
    },
    Fetch {
        #[arg(short, long)]
        group: Vec<Arc<str>>,

        spdx: Vec<Arc<str>>,
    },
    List {
        #[arg(short, long)]
        group: Vec<Arc<str>>,

        spdx: Vec<Arc<str>>,

        #[arg(long)]
        groups: bool,
    },
}
